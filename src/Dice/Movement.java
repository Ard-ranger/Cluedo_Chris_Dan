package Dice;

import Players.Player;
import Slot.Slot;

import java.util.Scanner;


public class Movement extends Dice {

//Top level move function
    public static int movement_action(Slot map[][], Player player_to_move) {
        System.out.println("You are currently at the position " + player_to_move.get_player_pawn().getxPosition() + "," + player_to_move.get_player_pawn().getyPosition());
        Scanner sca = new Scanner(System.in);
        System.out.println("Enter a direction u,d,l,r,t:");
        char ch = sca.next().charAt(0);
        int successful_move = 0;


        //5 movement choices: up, down, left, right, teleport
        switch (ch) {
            case 'u':
                successful_move = Movement.move_up(map, player_to_move);
                break;
            case 'd':
                successful_move = Movement.move_down(map, player_to_move);
                break;
            case 'l':
                successful_move = Movement.move_left(map, player_to_move);
                break;
            case 'r':
                successful_move = Movement.move_right(map, player_to_move);
                break;
            case 't':
                successful_move = Movement.teleport(map, player_to_move);
                break;

        }
        if (successful_move == 1) {
            return 1;
        }

        return 0;
    }

    public static int teleport(Slot map[][], Player my_player){
        int x = my_player.player_pawn.getxPosition();
        int y = my_player.player_pawn.getyPosition();
        int x_destination = map[x][y].getTeleport_to_x();
        int y_destination = map[x][y].getTeleport_to_y();
        boolean teleport_enabled = !map[x][y].getTeleport();

        System.out.println(x_destination + "," + y_destination);
        System.out.println(map[x][y].getTeleport());

        if (teleport_enabled) {
            System.out.println("Can't teleport from here. Try the corner rooms.");
            return 0;
        }

        if ((map[x_destination][y_destination].Get_player_container() != null)) {
            System.out.println("Can't teleport, pawn present in location.");
            return 0;
        }

        map[x][y].remove_player_container();
        map[x_destination][y_destination].Set_player_container(my_player.player_pawn);

        my_player.player_pawn.putxPosition(x_destination);
        my_player.player_pawn.putyPosition(y_destination);
        return 1;
    }



    public static int move_up(Slot map[][], Player my_player) {
        int x = my_player.player_pawn.getxPosition();
        int y = my_player.player_pawn.getyPosition();
        System.out.println(map[x][y].Get_player_container().name() + " was at location " + x + "," + y + " which is in the " + map[x][y].getName());

        //Checks if the destination is out of bounds. If so exits the function.
        if ((y+1) < 0 || (y+1) > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }

        //Checks if the destination is a wall. If so exits the function.
        if (map[x][y + 1].isIs_Wall()) {
            System.out.println("Can't move, wall present in destination");
            return 0;
        }

        if ((map[x][y + 1].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        //System.out.println(map[x][y].Get_player_container().name() + "At location " + x + "," + y);
        y = y + 1;
        if (y < 0 || y > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }

        //Checks if there is a player pawn present in the destination. If so exits the function.
        if ((map[x][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        map[x][y - 1].remove_player_container();

        if ((map[x][y].Get_player_container() == null)) {
            map[x][y].Set_player_container(my_player.player_pawn);
            System.out.println(map[x][y].Get_player_container().name() + " is now at location " + x + "," + y + " which is in the " + map[x][y].getName());
        }

        my_player.player_pawn.putyPosition(y);

        return 1;
    }


    public static int move_down(Slot map[][], Player my_player) {
        int x = my_player.player_pawn.getxPosition();
        int y = my_player.player_pawn.getyPosition();
        System.out.println(map[x][y].Get_player_container().name() + " was at location " + x + "," + y + " which is in the " + map[x][y].getName());


        //Checks if the destination is out of bounds. If so exits the function.
        if ((y-1) < 0 || (y-1) > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }


        //Checks if the destination is a wall. If so exits the function.
        if (map[x][y - 1].isIs_Wall()) {
            System.out.println("Can't move, wall present in destination");
            return 0;
        }

        if ((map[x][y - 1].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        //System.out.println(map[x][y].Get_player_container().name() + "At location " + x + "," + y);
        y = y - 1;



        if ((map[x][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        map[x][y + 1].remove_player_container();

        //Checks if there is a player pawn present in the destination. If so exits the function.
        if ((map[x][y].Get_player_container() == null)) {
            map[x][y].Set_player_container(my_player.player_pawn);
            System.out.println(map[x][y].Get_player_container().name() + " is now at location " + x + "," + y + " which is in the " + map[x][y].getName());
        }

        my_player.player_pawn.putyPosition(y);
        return 1;
    }

    public static int move_right(Slot map[][], Player my_player) {
        int x = my_player.player_pawn.getxPosition();
        int y = my_player.player_pawn.getyPosition();
        System.out.println(map[x][y].Get_player_container().name() + " was at location " + x + "," + y + " which is in the " + map[x][y].getName());


        //Checks if the destination is out of bounds. If so exits the function.
        if ((x+1) < 0 || (x+1) > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }


        if (map[x + 1][y].isIs_Wall()) {
            System.out.println("Can't move, wall present in destination");
            return 0;
        }

        if ((map[x + 1][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        //System.out.println(map[x][y].Get_player_container().name() + "At location " + x + "," + y);
        x = x + 1;
        if (x < 0 || x > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }

        //Checks if there is a player pawn present in the destination. If so exits the function.
        if ((map[x][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        map[x - 1][y].remove_player_container();


        if ((map[x][y].Get_player_container() == null)) {
            map[x][y].Set_player_container(my_player.player_pawn);
            System.out.println(map[x][y].Get_player_container().name() + " is now at location " + x + "," + y + " which is in the " + map[x][y].getName());
        }

        my_player.player_pawn.putxPosition(x);
        return 1;
    }

    public static int move_left(Slot map[][], Player my_player) {
        int x = my_player.player_pawn.getxPosition();
        int y = my_player.player_pawn.getyPosition();
        System.out.println(map[x][y].Get_player_container().name() + " was at location " + x + "," + y + " which is in the " + map[x][y].getName());


        //Checks if the destination is out of bounds. If so exits the function.
        if ((x-1) < 0 || (x-1) > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }


        //Checks if the destination is a wall. If so exits the function.
        if (map[x - 1][y].isIs_Wall()) {
            System.out.println("Can't move, wall present in destination");
            return 0;
        }

        //Checks if there is a player pawn present in the destination. If so exits the function.
        if ((map[x - 1][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        //System.out.println(map[x][y].Get_player_container().name() + "At location " + x + "," + y);
        x = x - 1;
        if (x < 0 || x > 22) {
            System.out.println("Can't move to " + x + "," + y + ", out of bounds");
            return 0;
        }

        if ((map[x][y].Get_player_container() != null)) {
            System.out.println("Can't move, pawn present in location");
            return 0;
        }

        map[x + 1][y].remove_player_container();

        if ((map[x][y].Get_player_container() == null)) {
            map[x][y].Set_player_container(my_player.player_pawn);
            System.out.println(map[x][y].Get_player_container().name() + " is now at location " + x + "," + y + " which is in the " + map[x][y].getName());
        }

        my_player.player_pawn.putxPosition(x);
        return 1;
    }

}
