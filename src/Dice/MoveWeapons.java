package Dice;

import Pawns.Weapon_pawn;
import Slot.Slot;

public class MoveWeapons {

    public static Weapon_pawn[] set_up_weapon_pawns(Slot map[][]) {
        Weapon_pawn candlestick = new Weapon_pawn("Candlestick", 3, 3, "\uD83D\uDD4E");
        Weapon_pawn knife = new Weapon_pawn("Knife", 12, 4, "\uD83D\uDDE1");
        Weapon_pawn leadpipe = new Weapon_pawn("Lead pipe", 21, 2, "﹄");
        Weapon_pawn revolver = new Weapon_pawn("Revolver", 3, 10, "\uD83D\uDD2B");
        Weapon_pawn rope = new Weapon_pawn("Rope", 21, 12, "～");
        Weapon_pawn wrench = new Weapon_pawn("Wrench", 3, 15, "\uD83D\uDD27");
        Weapon_pawn[] weapon_pawn_array;

        //Setting up the weapons
        weapon_pawn_array = new Weapon_pawn[6];
        weapon_pawn_array[0] = candlestick;
        weapon_pawn_array[1] = knife;
        weapon_pawn_array[2] = leadpipe;
        weapon_pawn_array[3] = revolver;
        weapon_pawn_array[4] = rope;
        weapon_pawn_array[5] = wrench;

        //Setting up the weapons default locations
        map[3][3].set_weapon_container(candlestick);
        map[12][4].set_weapon_container(knife);
        map[21][2].set_weapon_container(leadpipe);
        map[3][10].set_weapon_container(revolver);
        map[21][12].set_weapon_container(rope);
        map[3][15].set_weapon_container(wrench);

        return weapon_pawn_array;
    }

    public static void move_pawn_to_room(Slot map[][], String room, String weapon, Weapon_pawn[] weapon_pawns) {
        int new_x = 0;
        int new_y = 0;
        int old_x = 0;
        int old_y = 0;
        Weapon_pawn pawn_to_move = null;
        Weapon_pawn existing_pawn = null;

        //Finding the pawn in the array
        for (Weapon_pawn pawn : weapon_pawns) {
            if (pawn.name().equals(weapon)) {
                old_x = pawn.getxPosition();
                old_y = pawn.getyPosition();
                pawn_to_move = pawn;
                map[old_x][old_y].remove_weapon_container();
            }

        }


        //Each room has a preassigned slot to store the weapon array.
        switch (room.toLowerCase()) {
            case "conservatory":
                new_x = 3;
                new_y = 3;
                break;

            case "lounge":
                new_x = 21;
                new_y = 21;
                break;

            case "kitchen":
                new_x = 21;
                new_y = 3;
                break;

            case "library":
                new_x = 3;
                new_y = 15;
                break;

            case "hall":
                new_x = 12;
                new_y = 21;
                break;

            case "study":
                new_x = 3;
                new_y = 23;
                break;

            case "ballroom":
                new_x = 12;
                new_y = 4;
                break;

            case "dining room":
                new_x = 21;
                new_y = 12;
                break;

            case "billiard room":
                new_x = 3;
                new_y = 10;
                break;


        }

        //If a pawn is found successfully
        if (pawn_to_move != null) {

            //If there is a pawn already present at the desired location, swap the pawns.
            if (map[new_x][new_y].get_weapon_container() != null){
                //existing_pawn = map[new_x][new_y].get_weapon_container();
                map[old_x ][old_y].set_weapon_container(existing_pawn);
            }
            map[new_x][new_y].set_weapon_container(pawn_to_move);
        }
    }
}
