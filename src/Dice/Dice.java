package Dice;

import java.util.Random;


public class Dice {

    //Value of the dice face
    protected int value;

    //Initalises the dice
    public Dice() {
        value = roll_dice();
    }

    //Generates a roll of the dice
    public int roll_dice() {

        Random rand = new Random();
        int dice_face = rand.nextInt(6) + 1;
        return dice_face;

    }
}

