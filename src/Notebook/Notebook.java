package Notebook;

import java.util.ArrayList;
import java.util.List;

// Notebook class to hold the previous guesses of the player.
public class Notebook {

    List<String> notebook;

    // Initialise the notebook as an arraylist of strings
    public Notebook() {
        notebook = new ArrayList<String>();
    }

    // For adding entry to notebook
    public void add_to_notebook(String entry) {
        notebook.add(entry);
    }

    // Add a card shown to the players notebook. Different case statements for different
    public void add_refute_to_notebook(int type, String card_shown, List<String> guesses, int current_player_index, int refuting_player_index) {
        if (type == 0) {
            notebook.add("- I formulated the hypothesis that " + guesses.get(1));
            notebook.add(" committed murder in the " + guesses.get(0) + " with the " + guesses.get(2) + "\n");
            notebook.add("- Player " + (refuting_player_index + 1) + " refuted the hypothesis by showing the card \"" + card_shown + "\".\n");
        } else if (type == 1) {
            notebook.add("- Player " + current_player_index + " formulated the hypothesis that " + guesses.get(1));
            notebook.add(" committed murder in the " + guesses.get(0) + " with the " + guesses.get(2) + ".\n");
            notebook.add("- I refuted the hypothesis showing card \"" + card_shown + "\".\n");
        } else if (type == 2) {
            notebook.add("- Player " + current_player_index + " made the hypothesis that " + guesses.get(1));
            notebook.add(" committed murder in the " + guesses.get(0) + " with the " + guesses.get(2));
            notebook.add("- Player " + (refuting_player_index + 1) + " refuted the hypothesis by showing a card.\n");
        } else System.out.println("Error, type of refute must be in range 0-2");
    }

    // Show the players notebook
    public String show_notebook() {
    	String str = "";
        for (String i : notebook) {
            str += i + "\n";
        }
        return str;
    }

}