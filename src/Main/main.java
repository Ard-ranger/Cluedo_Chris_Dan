package Main;

import Cluedo.Cluedo;
import Gameboard.Gameboard;
import Slot.Slot;


public class main {

    public static void main(String[] args) {


        System.out.println("Candlestick - \uD83D\uDD4E\n" +
                "Knife - \uD83D\uDDE1\n" +
                "Lead pipe - ﹄\n" +
                "Revolver - \uD83D\uDD2B\n" +
                "Rope - ～\n" +
                "Wrench -\uD83D\uDD27 \n");

        //Gameboard gameboard = new Gameboard();
        Slot map[][] = new Slot[23][23];
        Gameboard.generate_board(map);
        // Begin cluedo game
        System.out.println("Cluedo");
        Cluedo cluedo = new Cluedo(map);
        //Display.print_board(map);

    }
}