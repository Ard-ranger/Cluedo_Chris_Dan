package Game;

import Cards.Card;
import Cards.CardsFactory;
import Envelope.Envelope;
import Pawns.PawnFactory;
import Pawns.Suspect_pawn;
import Pawns.Weapon_pawn;
import Players.Player;
import Players.PlayerFactory;
import org.apache.commons.lang.ArrayUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

// Game factory that prepares the players and the cards, and assigns cards to players
public class CreateGame {

    private int number_of_players;

    private CardsFactory cardFactory;
    private PlayerFactory playerfactory;
    private PawnFactory pawnfactory;
    private List<Card> game_cards;
    private List<Card> player_cards;
    private Envelope envelope;
    private List<Player> players;
    private List<Weapon_pawn> weapon_pawns;

    // Initialise cluedo game with initial conditions
    public CreateGame(int my_number_of_players) {
        number_of_players = my_number_of_players;

        cardFactory = new CardsFactory();
        playerfactory = new PlayerFactory(number_of_players);
        pawnfactory = new PawnFactory();
        game_cards = new ArrayList<Card>();
        player_cards = new ArrayList<Card>();

        envelope = new Envelope();
        players = new ArrayList<Player>();
    }

    private static List<Card> concatenate(Card[] a, Card[] b, Card[] c) {
        List<Card> game_cards = new ArrayList<Card>(a.length + b.length + c.length);

        for (Card x : a) {
            game_cards.add(x);
        }

        for (Card y : b) {
            game_cards.add(y);
        }

        for (Card z : c) {
            game_cards.add(z);
        }
        return game_cards;
    }

    // Initialises the cards in the game and places three random cards in the envelope to represent
    // the murder room, suspect and weapon. It then shuffles the remaining cards.
    public void prepare_cards() {
        Card[] room_cards = cardFactory.getCards("Room");
        Card[] suspect_cards = cardFactory.getCards("Suspects");
        Card[] weapon_cards = cardFactory.getCards("Weapons");

        game_cards = concatenate(room_cards, suspect_cards, weapon_cards);

        // Random integer to choose a random card
        Random generator = new Random();
        int room_rand = generator.nextInt(9);
        int suspect_rand = generator.nextInt(6);
        int weapon_rand = generator.nextInt(6);
        
        // For picking cards randomly
        envelope.add_card(room_cards[room_rand]);
        envelope.add_card(suspect_cards[suspect_rand]);
        envelope.add_card(weapon_cards[weapon_rand]);

        room_cards = (Card[]) ArrayUtils.remove(room_cards, room_rand);
        suspect_cards = (Card[]) ArrayUtils.remove(suspect_cards, suspect_rand);
        weapon_cards = (Card[]) ArrayUtils.remove(weapon_cards, weapon_rand);

        player_cards = concatenate(room_cards, suspect_cards, weapon_cards);
        Collections.shuffle(player_cards);
    }

    // Initialises all the players in the game
    public void prepare_players(List<String> player_names) {
        playerfactory.add_players(player_names);
        players = playerfactory.get_players();
    }

    // Assign cards to each player in the game
    public void assign_cards_to_players() {
        int j = 0;

        if (players.isEmpty()) {
            System.out.println("Players have not been created");
        }
        
        for (int player = 0; player < number_of_players; player++) {
        	players.get(player).add_to_notebook("\nThe cards I have are:");
        }

        for (int i = player_cards.size() - 1; i >= 0; i--) {
            players.get(j).assign_card(player_cards.get(i));
            player_cards.remove(i);
            if (j == number_of_players - 1) {
                j = 0;
                continue;
            }
            j++;
        }
    }

    // Handle game events such as notebook additions
    public boolean game_event(int guessing_player_index, List<Player> game_players, List<String> guesses) {
        boolean rev = false;

        // Go around each player playing the game, wrap around if player index not 0, returns true if a player refuted a hypothesis
        for (int refuting_player = guessing_player_index + 1; refuting_player < game_players.size(); refuting_player++) {

            if (refuting_player == guessing_player_index && rev)
                break;

            // Check next player if they have of the cards, if they do then add event to notebooks
            for (int guess = 0; guess < guesses.size(); guess++) {
                if (game_players.get(refuting_player).have_card(guesses.get(guess))) {
                    System.out.println(guesses.get(guess));
                    game_players.get(guessing_player_index).add_refute_to_notebook(0, guesses.get(guess),
                            guesses, guessing_player_index, refuting_player);
                    game_players.get(refuting_player).add_refute_to_notebook(1, guesses.get(guess),
                            guesses, guessing_player_index, refuting_player);
                    add_other_player_events(game_players, guessing_player_index, refuting_player, guesses);
                    return true;
                }
            }

            if (refuting_player == game_players.size() - 1) {
                refuting_player = -1;
                rev = true;
            }
        }
        return false;
    }

    public List<Card> get_game_cards() {
        return game_cards;
    }

    public List<Card> get_player_cards() {
        return player_cards;
    }

    public Envelope get_envelope() {
        return envelope;
    }

    public List<Player> get_players() {
        return players;
    }


    // Private function to handle notebook additions of non refuting and guessing players
    private void add_other_player_events(List<Player> game_players,
                                         int guessing_player_index,
                                         int refute_player_index,
                                         List<String> guesses) {

        for (int player = 0; player < game_players.size(); player++) {
            if (player == guessing_player_index || player == refute_player_index)
                continue;
            else {
                // Add an observer refute to each of the other players
                game_players.get(player).add_refute_to_notebook(2, "", guesses, guessing_player_index, refute_player_index);
            }
        }
    }
}
