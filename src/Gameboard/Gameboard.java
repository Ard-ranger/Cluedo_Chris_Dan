package Gameboard;

import Slot.Slot;

public class Gameboard {

    public static void generate_board(Slot map[][]) {
        //Generating test board.
        int x = 0;
        int y = 0;
        while (x < 23) {
            while (y < 23) {
                boolean wall = false;
                boolean teleport = false;
                int tele_x_co = 0;
                int tele_y_co = 0;
                String room;
                if ((x >= 0 && x <= 7) && (y >= 19 && y <= 22)) {
                    room = "Study";
                    wall = true;
                } else if ((x >= 10 && x <= 15) && (y >= 18 && y <= 22)) {
                    room = "Hall";
                    wall = true;
                } else if ((x >= 18 && x <= 22) && (y >= 18 && y <= 22)) {
                    room = "Lounge";
                    wall = true;
                } else if ((x >= 17 && x <= 22) && (y >= 9 && y <= 15)) {
                    room = "Dining room";
                    wall = true;
                } else if ((x >= 19 && x <= 22) && (y >= 0 && y <= 6)) {
                    room = "Kitchen";
                    wall = true;
                } else if ((x >= 11 && x <= 14) && (y >= 0 && y <= 7)) {
                    room = "Ballroom";
                    wall = true;
                } else if ((x >= 0 && x <= 6) && (y >= 0 && y <= 5)) {
                    room = "Conservatory";
                    wall = true;
                } else if ((x >= 0 && x <= 6) && (y >= 8 && y <= 12)) {
                    room = "Billiard room";
                    wall = true;
                } else if ((x >= 0 && x <= 7) && (y >= 14 && y <= 16)) {
                    room = "Library";
                    wall = true;
                } else {
                    room = "Corridor";
                    wall = false;
                }


                if (x == 6 && y == 5) {
                    wall = false;
                    teleport = true;
                    tele_x_co = 18;
                    tele_y_co = 18;
                }

                if (x == 6 && y == 8) {
                    wall = false;
                }

                if (x == 6 && y == 12) {
                    wall = false;
                }

                if (x == 7 && y == 14) {
                    wall = false;
                }

                if (x == 7 && y == 16) {
                    wall = false;
                }

                if (x == 7 && y == 19) {
                    wall = false;
                    teleport = true;
                    tele_x_co = 19;
                    tele_y_co = 6;
                }

                if (x == 10 && y == 18) {
                    wall = false;
                }

                if (x == 11 && y == 7) {
                    wall = false;
                }

                if (x == 14 && y == 7) {
                    wall = false;
                }

                if (x == 15 && y == 18) {
                    wall = false;
                }

                if (x == 17 && y == 9) {
                    wall = false;
                }

                if (x == 17 && y == 15) {
                    wall = false;
                }

                if (x == 18 && y == 18) {
                    wall = false;
                    teleport = true;
                    tele_x_co = 6;
                    tele_y_co = 5;
                }

                if (x == 19 && y == 6) {
                    wall = false;
                    teleport = true;
                    tele_x_co = 7;
                    tele_y_co = 19;
                }

                map[x][y] = new Slot(room, wall, x, y, teleport, tele_x_co, tele_y_co);


                y = y + 1;
            }
            x = x + 1;
            y = 0;
        }
    }
}