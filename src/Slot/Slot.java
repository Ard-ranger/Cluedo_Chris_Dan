package Slot;


import Pawns.Pawn;
import Pawns.Suspect_pawn;
import Pawns.Weapon_pawn;


public class Slot {

    private Weapon_pawn weapon_Pawn[] = new Weapon_pawn[1];
    private Suspect_pawn player_Pawn[] = new Suspect_pawn[1];
    private String room;
    private boolean is_Wall;
    private boolean can_teleport;
    private int teleport_to_x;
    private int teleport_to_y;

    public Slot(String name, boolean wall, int x_coordinate, int y_coordinate, boolean teleport, int to_x, int to_y) {
        room = name;
        is_Wall = wall;
        Pawn[] weapon_Pawn;
        weapon_Pawn = new Pawn[1];
        Suspect_pawn[] player_Pawn;
        player_Pawn = new Suspect_pawn[1];
        can_teleport = teleport;
        teleport_to_x = to_x;
        teleport_to_y = to_y;
    }

    public void set_weapon_container(Weapon_pawn input_weapon_pawn) {
        weapon_Pawn[0] = input_weapon_pawn;
    }

    public Pawn get_weapon_container() {
        return weapon_Pawn[0];
    }

    public void Set_player_container(Suspect_pawn input_player_pawn) {
        player_Pawn[0] = input_player_pawn;

    }

    public Pawn Get_player_container() {
        return player_Pawn[0];
    }

    public void remove_weapon_container() {
        weapon_Pawn[0] = null;
    }

    public void remove_player_container() {
        player_Pawn[0] = null;
    }

    //Gets the room name
    public String getName() {
        return room;
    }

    //Gets the player number from the players pawn
    public int get_index() {
        return player_Pawn[0].get_index();
    }

    public String get_weapon_icon() {
            return weapon_Pawn[0].getIcon();
    }


    public boolean isIs_Wall() {
        return is_Wall;
    }


    public int getTeleport_to_x() {
        return teleport_to_x;
    }

    public int getTeleport_to_y() {
        return teleport_to_y;
    }

    public boolean getTeleport() {
        return can_teleport;
    }
}
