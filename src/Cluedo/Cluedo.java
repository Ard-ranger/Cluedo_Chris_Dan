package Cluedo;

import Cards.Card;
import Dice.*;
import Display.Display;
import Envelope.Envelope;
import Game.CreateGame;
import Pawns.*;
import Players.Player;
import Slot.Slot;



import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cluedo {
    public Cluedo(Slot map[][]) {
        // Initialize variables needed
        Scanner my_scanner = new Scanner(System.in);
        List<String> player_names = new ArrayList<String>();
        int number_of_players = 0;
        boolean accusation;
        boolean game_over = false;
        boolean correct_input = false;
        boolean room_valid;
        boolean suspect_valid;
        boolean weapon_valid;
        boolean valid;


        // Check input is correct
        while (!correct_input) {
            System.out.println("Enter number of players: ");
            number_of_players = my_scanner.nextInt();

            if (number_of_players > 6) {
                System.out.println("Error: Maximum 6 players");
                return;
            }
            else if (number_of_players <= 2) {
                System.out.println("Error: Minimum of 3 players");
                return;
            }

            else {
                correct_input = true;
            }
        }

        // Initialising scanner object
        my_scanner.nextLine();

        // Add names to each player
        for (int i = 0; i < number_of_players; i++) {
            System.out.print("Input player name for player " + i + ": ");
            String player_name = my_scanner.nextLine();
            player_names.add(player_name);
        }


        // Prepare the game
        CreateGame game = new CreateGame(number_of_players);
        game.prepare_cards();

        game.prepare_players(player_names);
        game.assign_cards_to_players();

        List<Card> game_cards = game.get_game_cards();
        Envelope envelope = game.get_envelope();
        List<Player> game_players = game.get_players();
        //List<Weapon_pawn> weapon_pawns = game.get_weapon_pawns();

        Weapon_pawn[] weapon_pawns = MoveWeapons.set_up_weapon_pawns(map);

        //Initialising player pawns on the board
        for (int i = 0; i < number_of_players; i++) {
            int x = game_players.get(i).get_player_pawn().getxPosition();
            int y = game_players.get(i).get_player_pawn().getyPosition();
            map[x][y].Set_player_container(game_players.get(i).get_player_pawn());
        }

        for (int player = 0; player < number_of_players; player++) {
        	game_players.get(player).add_to_notebook("\nGame Events:");
        }
        
        //Displays the solution to the murder
        envelope.print_envelope();


        // Game loop. Continues until a player wins or all players have been eliminated
        while (!game_over) {
            // Each player
            for (int player = 0; player < number_of_players; player++) {
                //Testing to see if each player is still in the game
                if (game_players.get(player).is_playing()) {

                    List<String> guesses = new ArrayList<String>();
                    Dice my_die = new Dice();
                    int number_of_actions = my_die.roll_dice();

                    System.out.println("Your dice roll was " + number_of_actions);

                    while (number_of_actions > 0) {
                        Display.print_unicode(map);
                        System.out.println("\nPlayer " + (player + 1) + ": " + game_players.get(player).get_name());
                        System.out.println("Player pawn: " + game_players.get(player).get_player_pawn().name());

                        System.out.println("You have " + number_of_actions + " actions remaining");
                        String current_room = map[game_players.get(player).get_player_pawn().getxPosition()][game_players.get(player).get_player_pawn().getyPosition()].getName();

                        Scanner scan = new Scanner(System.in);
                        System.out.println("Would you like to move,show notebook or guess? (m/s/g):");
                        char action = scan.next().charAt(0);

                        if (action == 'm') {
                            int successful_move = Movement.movement_action(map, game_players.get(player));
                            if (successful_move == 1)
                                number_of_actions = number_of_actions - 1;
                        }

                        if (action == 's') {
                            System.out.println(game_players.get(player).get_notebook());
                        }

                        if (action == 'g' && current_room == "Corridor"){
                            System.out.println("Can't formulate a hypothesis in the corridor. Please enter a room.");
                        }

                        if (action == 'g' && current_room != "Corridor") {

                            // Make guesses
                            System.out.println("Room guess: " + current_room);
                            //System.out.println(is_valid_card(current_room, game_cards));
                            System.out.println("Suspect guess: ");
                            String suspect_guess = my_scanner.nextLine();

                            System.out.println("Weapon guess: ");
                            String weapon_guess = my_scanner.nextLine();

                            //Testing to see if the input is valid.
                            room_valid = is_valid_card(current_room, game_cards);
                            suspect_valid = is_valid_card(suspect_guess, game_cards);
                            weapon_valid = is_valid_card(weapon_guess, game_cards);

                            valid = room_valid && suspect_valid && weapon_valid;
                            // If cards are valid, continue
                            if (valid) {
                                //Setting number of further actions on the turn to 0.
                                number_of_actions = 0;
                                guesses.add(current_room);
                                guesses.add(suspect_guess);
                                guesses.add(weapon_guess);
                                MoveWeapons.move_pawn_to_room(map, current_room, weapon_guess, weapon_pawns);

                                accusation = game.game_event(player, game_players, guesses);

                                // Make an accusation if player cannot be refuted
                                if (!accusation) {
                                    // Ask user for accusation
                                    System.out.println("Input room accusation: " + current_room);

                                    System.out.println("Input suspect accusation: ");
                                    String suspect_accusation = my_scanner.nextLine();

                                    System.out.println("Input weapon accusation: ");
                                    String weapon_accusation = my_scanner.nextLine();

                                    // Concatenate accusations for ease of use
                                    String concat_accusation = current_room + suspect_accusation + weapon_accusation;

                                    // Check guesses
                                    if (concat_accusation.equalsIgnoreCase(envelope.check_envelope())) {
                                        System.out.println("The guess is correct");
                                        System.out.println("Player " + player + "wins!");
                                        game_over = true;
                                    } else {
                                        System.out.println("The guess is incorrect");
                                        game_players.get(player).player_lost();
                                        System.out.println(game_players.get(player).get_name() + " is out of the game");
                                        map[game_players.get(player).get_player_pawn().getxPosition()][game_players.get(player).get_player_pawn().getyPosition()].remove_player_container();
                                        break;
                                    }
                                }
                            } else {
                                System.out.println("Cards not valid");
                                break;
                            }

                        }
                    }


                }
                else {
                    System.out.println("All players are eliminated, GAME OVER");
                    game_over = true;
                }
            }
        }
        my_scanner.close();
    }

    // Checks that a card is valid against all game cards
    private static boolean is_valid_card(String my_guess, List<Card> my_game_cards) {
        for (Card i : my_game_cards) {
            if (i.name().equalsIgnoreCase(my_guess))
                return true;
        }
        return false;
    }
}
