package Cards;

// Suspect_card class that is a subclass of card
public class Suspect_Card extends Card {

    public Suspect_Card(String input_name) {
        super("Suspect", input_name);
    }

    public String name() {
        return super.name;
    }

    public String type() {
        return super.type;
    }

}
