package Cards;

// Abstract class to represent the different types of game cards. Attributes name and type.
public abstract class Card {

    protected String type;
    protected String name;

    public Card(String my_type, String my_name) {
        type = my_type;
        name = my_name;
    }

    //Card name
    abstract public String name();

    //Card type Room/Suspect/weapon
    abstract public String type();
}
