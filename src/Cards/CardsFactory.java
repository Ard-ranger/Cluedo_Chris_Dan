package Cards;

// Factory that initialises the different types of game cards, depending on the getCards parameter
public class CardsFactory {
    public Card[] getCards(String cardTypes) {

        // If no cardType is given, then return null

        // If the desired card type is ROOM then create the room cards
        if (cardTypes.equalsIgnoreCase("ROOM")) {
            Card conservatory = new Room_Card("Conservatory");
            Card lounge = new Room_Card("Lounge");
            Card kitchen = new Room_Card("Kitchen");
            Card library = new Room_Card("Library");
            Card hall = new Room_Card("Hall");
            Card study = new Room_Card("Study");
            Card ballroom = new Room_Card("Ballroom");
            Card dining = new Room_Card("Dining room");
            Card billiard = new Room_Card("Billiard room");

            // Place all room cards in an array
            Card[] room_cards = {conservatory, lounge, kitchen, library, hall, study, ballroom, dining, billiard};
            return room_cards;
        }

        // If the desired card type is suspects then create the suspect cards
        else if (cardTypes.equalsIgnoreCase("SUSPECTS")) {
            Card plum = new Suspect_Card("Professor Plum");
            Card green = new Suspect_Card("Reverend Green");
            Card mustard = new Suspect_Card("Colonel Mustard");
            Card peacock = new Suspect_Card("Mrs. Peacock");
            Card white = new Suspect_Card("Mrs. White");
            Card scarlett = new Suspect_Card("Miss Scarlett");

            // Place all suspect cards in an array
            Card[] suspect_cards = {plum, green, mustard, peacock, white, scarlett};
            return suspect_cards;
        }

        // If the desired card type is weapons, then create the weapon cards
        else if (cardTypes.equalsIgnoreCase("WEAPONS")) {
            Card candlestick = new Weapon_Card("Candlestick");
            Card knife = new Weapon_Card("Knife");
            Card leadpipe = new Weapon_Card("Lead pipe");
            Card revolver = new Weapon_Card("Revolver");
            Card rope = new Weapon_Card("Rope");
            Card wrench = new Weapon_Card("Wrench");

            // Place all weapon cards in an array
            Card[] weapon_cards = {candlestick, knife, leadpipe, revolver, rope, wrench};
            return weapon_cards;
        } else {
            return null;
        }
    }
}