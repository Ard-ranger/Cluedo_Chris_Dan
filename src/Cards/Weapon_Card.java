package Cards;

// Weapon_card class that is a subclass of card.
public class Weapon_Card extends Card {

    public Weapon_Card(String input_name) {
        super("Weapon", input_name);
    }

    public String name() {
        return super.name;
    }

    public String type() {
        return super.type;
    }
}
