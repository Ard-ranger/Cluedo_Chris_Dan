package Cards;


// Room_card class that is a subclass of Card
public class Room_Card extends Card {

    public Room_Card(String input_name) {
        super("Room", input_name);
    }

    public String name() {
        return super.name;
    }

    public String type() {
        return super.type;
    }

}