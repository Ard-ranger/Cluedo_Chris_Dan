package Pawns;

// Pawn abstract class to represent the game pawns within the game. Each pawn has a type, name, x-co-ordinate, y-co-ordinate
// position on the board
public abstract class Pawn {

    protected String type; //Return pawn class
    protected String name;  //Name of the pawn
    protected int x_pos;    //Return the x position of the pawn
    protected int y_pos;    //Return the y position of the pawn

    public Pawn(String my_type, String my_name, int my_x, int my_y) {
        type = my_type;
        name = my_name;
        x_pos = my_x;
        y_pos = my_y;
    }

    // Abstract getters to return attributes.
    abstract public String type();

    abstract public String name();

    abstract public int getxPosition();

    abstract public int getyPosition();

    //abstract public int putxPosition(int new_x_pos);

    //abstract public int putyPosition(int new_y_pos);
}
