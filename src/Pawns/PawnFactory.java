package Pawns;

import java.util.ArrayList;
import java.util.List;

public class PawnFactory {
    public Suspect_pawn get_suspect_pawns(int i) {
        Suspect_pawn plum = new Suspect_pawn("Professor Plum", 10, 0, 1);
        Suspect_pawn green = new Suspect_pawn("Reverend Green", 15, 0, 2);
        Suspect_pawn mustard = new Suspect_pawn("Colonel Mustard", 22, 17, 3);
        Suspect_pawn peacock = new Suspect_pawn("Mrs. Peacock", 17, 22, 4);
        Suspect_pawn white = new Suspect_pawn("Mrs. White", 0, 18, 5);
        Suspect_pawn scarlett = new Suspect_pawn("Miss Scarlett", 0, 6, 6);

        // Place all suspect cards in an array
        List<Suspect_pawn> suspect_pawns = new ArrayList<Suspect_pawn>();
        suspect_pawns.add(plum);
        suspect_pawns.add(green);
        suspect_pawns.add(mustard);
        suspect_pawns.add(peacock);
        suspect_pawns.add(white);
        suspect_pawns.add(scarlett);

        return suspect_pawns.get(i);
    }

    public Weapon_pawn get_weapon_pawns(int i) {
        Weapon_pawn candlestick = new Weapon_pawn("Candlestick", 0, 0,"\uD83D\uDD4E");
        Weapon_pawn knife = new Weapon_pawn("Knife", 0, 1,"\uD83D\uDDE1");
        Weapon_pawn leadpipe = new Weapon_pawn("Lead pipe", 1, 1,"|");
        Weapon_pawn revolver = new Weapon_pawn("Revolver", 2, 0,"\uD83D\uDD2B");
        Weapon_pawn rope = new Weapon_pawn("Rope", 2, 1,"୪");
        Weapon_pawn wrench = new Weapon_pawn("Wrench", 2, 2,"\uD83D\uDD27");

        // Place all weapon cards in an array
        List<Weapon_pawn> weapon_pawns = new ArrayList<Weapon_pawn>();
        weapon_pawns.add(candlestick);
        weapon_pawns.add(knife);
        weapon_pawns.add(leadpipe);
        weapon_pawns.add(revolver);
        weapon_pawns.add(rope);
        weapon_pawns.add(wrench);

        return weapon_pawns.get(i);
    }
}
