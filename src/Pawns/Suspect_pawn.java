package Pawns;

// Suspect_pawn class that represents the suspect pawns in the game, derived from pawn class
public class Suspect_pawn extends Pawn {

    // Constructor sets the pawn name, type, position and player index
    private int player_number;

    public Suspect_pawn(String name, int x, int y, int index) {
        super("Suspect", name, x, y);
        player_number = index;
    }


    public String type() {
        return super.type;
    }

    public String name() {
        return super.name;
    }

    public int xPosition() {
        return super.x_pos;
    }

    public int yPosition() {
        return super.y_pos;
    }

    public int get_index() {
        return player_number;
    }

    @Override
    public int getxPosition() {
        return x_pos;
    }

    @Override
    public int getyPosition() {
        return y_pos;
    }

    public void putxPosition(int new_x_pos) {
        x_pos = new_x_pos;
    }

    public void putyPosition(int new_y_pos) {
        y_pos = new_y_pos;
    }

}
