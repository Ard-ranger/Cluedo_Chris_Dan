package Pawns;

// Weapon_pawn class that derives from pawn
public class Weapon_pawn extends Pawn {


    private String icon;

    // Constructor sets the type, name and position on the board
    public Weapon_pawn(String name, int x, int y,  String input_icon) {
        super("Weapon", name, x, y);
        this.icon = input_icon;
    }

    public String type() {
        return super.type;
    }

    public String name() {
        return super.name;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public int getxPosition() {
        return super.x_pos;
    }

    @Override
    public int getyPosition() {
        return super.y_pos;
    }
}