package Players;

import java.util.ArrayList;
import java.util.List;

// Factory to create each individual player and then add them to the game. 
public class PlayerFactory {

    private int number_of_players;            // Number of players playing

    private List<Player> players;            // A list of game players

    // Constructor initialised with game_cards, player_cards and number of players
    public PlayerFactory(int my_number_of_players) {
        number_of_players = my_number_of_players;

        players = new ArrayList<Player>(my_number_of_players);
    }

    // Add players to the game
    public void add_players(List<String> player_names) {
        for (int i = 0; i < number_of_players; i++) {
            Player my_player = new Player(i, player_names.get(i));
            players.add(my_player);
        }
    }

    // Get all the players from the game
    public List<Player> get_players() {
        return players;
    }

}
