package Players;

import Cards.Card;
import Notebook.Notebook;
import Pawns.PawnFactory;
import Pawns.Suspect_pawn;

import java.util.ArrayList;
import java.util.List;


// Player class that represents each player
public class Player {

    public Suspect_pawn player_pawn;    // The pawn each player is assigned
    private List<Card> player_cards;    // The cards each player holds in their hand
    private Notebook notebook;            // Each players notebook
    private boolean is_playing;
    private String player_name;
    private PawnFactory pawnfactory;

    // Constructor takes in the index of the player and initialises player_cards,
    // suspect_pawn and notebook
    public Player(int my_index, String my_player_name) {
        player_cards = new ArrayList<Card>();
        pawnfactory = new PawnFactory();
        player_name = my_player_name;
        player_pawn = pawnfactory.get_suspect_pawns(my_index);
        notebook = new Notebook();
        is_playing = true;
    }

    // Assign a card to be added to the players hand, add that card to their
    // notebook also
    public void assign_card(Card my_players_card) {
        player_cards.add(my_players_card);
        notebook.add_to_notebook(my_players_card.name());
    }

    public boolean have_card(String guess) {
        for (int i = 0; i < player_cards.size(); i++) {
            if (player_cards.get(i).name().equalsIgnoreCase(guess)) {
                return true;
            }
        }
        return false;
    }

    public void set_suspect_pawn_position(int x, int y) {
        player_pawn.putxPosition(x);
        player_pawn.putyPosition(y);
    }

    // Show the players cards
    public String get_cards() {
        String str = "";
        for (Card i : player_cards) {
            str += i.name() + " " + i.type() + "\n";
        }
        return str;
    }

    public void add_to_notebook(String card) {
        notebook.add_to_notebook(card);
    }

    // Add a card shown to their notebook
    public void add_refute_to_notebook(int type, String card_shown, List<String> guesses, int player_index, int refuting_player_index) {
        notebook.add_refute_to_notebook(type, card_shown, guesses, player_index, refuting_player_index);
    }

    // Show the players notebook
    public String get_notebook() {
        return notebook.show_notebook();
    }

    public Suspect_pawn get_player_pawn() {
        return player_pawn;
    }

    // Player lost the game
    public void player_lost() {
        is_playing = false;
    }

    // Player is playing the game
    public boolean is_playing() {
        return is_playing;
    }

    // Get player name
    public String get_name() {
        return player_name;
    }
    
    public int get_number_of_cards() {
    	return player_cards.size();
    }
}
