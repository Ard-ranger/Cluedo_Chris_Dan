package Tests;

import Slot.*;
import Pawns.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class SlotTest {

    private Slot my_wall_slot;
    private Slot my_tile_slot;
    private Weapon_pawn my_weapon_pawn;
    private Suspect_pawn my_suspect_pawn;




    @BeforeClass
    public static void setupclass() {
        System.out.println("Setting up Slot test class");
    }

    @Before
    public void setUp() {
        my_wall_slot = new Slot("Hall",true,1,1,false,1,1);
        my_tile_slot = new Slot("Ballroom",false,1,1,false,1,1);
        my_weapon_pawn = new Weapon_pawn("Knife", 1 , 2, "\uD83D\uDDE1");
        my_suspect_pawn = new Suspect_pawn("Professor Plum", 1 , 2, 0);
    }

    @Test
    public void set_weapon_container() {
        assertEquals("Non null returned from empty tile", my_tile_slot.get_weapon_container(), null);
        my_tile_slot.set_weapon_container(my_weapon_pawn);
        assertEquals("Incorrect weapon pawn on tile", my_tile_slot.get_weapon_container(), my_weapon_pawn);
    }

    @Test
    public void set_player_container() {
        assertEquals("Non null returned from empty tile", my_tile_slot.Get_player_container(), null);
        my_tile_slot.Set_player_container(my_suspect_pawn);
        assertEquals("Incorrect weapon pawn on tile", my_tile_slot.Get_player_container(), my_suspect_pawn);
    }

    @Test
    public void get_player_container() {
        assertEquals("Non null returned from empty tile", my_tile_slot.Get_player_container(), null);
        my_tile_slot.Set_player_container(my_suspect_pawn);
        assertEquals("Incorrect weapon pawn on tile", my_tile_slot.Get_player_container(), my_suspect_pawn);
    }

    @Test
    public void get_index() {
        my_tile_slot.Set_player_container(my_suspect_pawn);
        assertEquals("Incorrect index from pawn on tile", my_tile_slot.get_index(), 0);
    }

    @Test
    public void remove_weapon_container() {

    }

    @Test
    public void remove_player_container() {
        assertEquals("Non null returned from empty tile", my_tile_slot.Get_player_container(), null);
        my_tile_slot.Set_player_container(my_suspect_pawn);
        assertEquals("Incorrect weapon pawn on tile", my_tile_slot.Get_player_container(), my_suspect_pawn);
        my_tile_slot.remove_player_container();
        assertEquals("Non null returned from empty tile", my_tile_slot.Get_player_container(), null);
    }

    @Test
    public void get_name() {
        assertEquals("Slot.name error", my_tile_slot.getName(), "Ballroom");
    }


    @Test
    public void isIs_Wall() {
        //my_wall_slot = new Slot("Hall",true,1,1,false,1,1);
        //my_tile_slot = new Slot("Hall",false,1,1,false,1,1);

        assertEquals("Wall does not exist and should", my_wall_slot.isIs_Wall(), true);
        assertEquals("Wall exists and should not", my_tile_slot.isIs_Wall(), false);
    }

    @After
    public void tearDown() {
        my_tile_slot = null;
        my_wall_slot = null;
        my_suspect_pawn = null;
        my_weapon_pawn = null;
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Tearing down Slot Tests");
    }

}