package Tests;

import Cards.Suspect_Card;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Suspect_Card_Tests {
	
	private Suspect_Card my_suspect_card;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Suspect_Card test class");
	}
	
	@Before
	public void setUp() {
		my_suspect_card = new Suspect_Card("Professor Plum");
	}
	
	@After
	public void tearDown() {
		my_suspect_card = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Suspect_Card Tests");
	}

	@Test
	public void test_suspect_card_name() {
		assertEquals("Incorrect name assignment to suspect card", my_suspect_card.name(), "Professor Plum");
		
	}
	
	@Test
	public void test_suspect_card_type() {
		assertEquals("Incorrect type assignment to suspect card", my_suspect_card.type(), "Suspect");
	}
}
