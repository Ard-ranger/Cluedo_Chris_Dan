package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Players.*;

public class PlayerFactory_Tests {
	
	private PlayerFactory playerfactory;
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up playerfactory test class");
	}
	
	@Before
	public void setUp() {
		playerfactory = new PlayerFactory(3);
	}
	
	@After
	public void tearDown() {
		playerfactory = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down PlayerFactory Tests");
	}
	
	
	@Test
	public void test_player_creation() {
		List<String> names = new ArrayList<String>();
		names.add("Chris");
		names.add("Dan");
		names.add("Phil");
		playerfactory.add_players(names);
		
		assertEquals("Incorrect player creation", playerfactory.get_players().get(0).get_name(), "Chris");
		assertEquals("Incorrect player creation", playerfactory.get_players().get(1).get_name(), "Dan");
		assertEquals("Incorrect player creation", playerfactory.get_players().get(2).get_name(), "Phil");
		assertEquals("Incorrect player creation", playerfactory.get_players().get(0).get_player_pawn().name(), "Professor Plum");
		assertEquals("Incorrect player creation", playerfactory.get_players().get(1).get_player_pawn().name(), "Reverend Green");
		assertEquals("Incorrect player creation", playerfactory.get_players().get(2).get_player_pawn().name(), "Colonel Mustard");
	}

}
