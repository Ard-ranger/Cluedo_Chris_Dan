package Tests;


import Cards.Room_Card;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Room_Card_Tests {
	
	private Room_Card my_room_card;
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Room_Card test class");
	}
	
	@Before
	public void setUp() {
		my_room_card = new Room_Card("Hall");
	}
	
	@After
	public void tearDown() {
		my_room_card = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Room Card Tests");
	}
	
	@Test
	public void test_name() {
		assertEquals( "Incorrect name assignment to room card", my_room_card.name(), "Hall");
	}
	
	@Test
	public void test_type() {
		assertEquals( "Incorrect type assigned to room card", my_room_card.type(), "Room");
	}
}
