package Tests;
import Dice.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class DiceTest {


    private Dice my_dice;
    @BeforeClass
    public static void setupclass() {
        System.out.println("Setting up Dice test class");
    }


    @Before
    public void setUp() {
        my_dice = new Dice();
    }

    @Test
    public void  roll_dice() {
        int my_num;
        my_num  = my_dice.roll_dice();
        assertTrue("Error, dice value is too high", 6 >= my_num);
        assertTrue("Error, dice value is too low",  1 <= my_num);    }

    @After
    public void tearDown() {
        my_dice = null;
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Tearing down Dice Tests");
    }


}