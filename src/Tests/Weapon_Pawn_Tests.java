package Tests;

import Pawns.Weapon_pawn;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Weapon_Pawn_Tests {

private Weapon_pawn my_weapon_pawn;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Weapon_Pawn test class");
	}
	
	@Before
	public void setUp() {
		my_weapon_pawn = new Weapon_pawn("Knife", 1 , 2, "\uD83D\uDDE1");
	}
	
	@After
	public void tearDown() {
		my_weapon_pawn = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Weapon_Pawn Tests");
	}
	
	@Test
	public void test_weapon_pawn_type() {
		assertEquals("Incorrect type assignment to weapon pawn", my_weapon_pawn.type(), "Weapon");
	}
	
	@Test
	public void test_weapon_pawn_name() {
		assertEquals("Incorrect name assignment to suspect pawn", my_weapon_pawn.name(), "Knife");
	}
	
	@Test
	public void test_weapon_pawn_correct_starting_position() {
		assertEquals("Incorrect x starting position for suspect pawn", my_weapon_pawn.getxPosition(), 1);
		assertEquals("Incorrect y starting position for suspect pawn", my_weapon_pawn.getyPosition(), 2);
	}
}
