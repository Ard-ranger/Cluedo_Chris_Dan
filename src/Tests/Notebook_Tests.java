package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import Notebook.*;
import org.junit.Test;

import Game.CreateGame;

public class Notebook_Tests {

	private Notebook my_notebook;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Notebook Test");
	}
	
	@Before
	public void setUp() {
		my_notebook = new Notebook();
	}
	
	@After
	public void tearDown() {
		my_notebook = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Notebook Tests");
	}
	
	@Test
	public void test_notebook_addition() {
		my_notebook.add_to_notebook("Chris");
		assertEquals("Incorrect Notebook addition", my_notebook.show_notebook(), "Chris\n");
	}
	
	@Test
	public void test_adding_refute() {
		List<String> guesses = new ArrayList<String>();
		String concat  = new String();
		guesses.add("Hall");
		guesses.add("Chris");
		guesses.add("Knife");
		my_notebook.add_refute_to_notebook(0, "Card shown", guesses, 0, 1);
		concat += ("- I formulated the hypothesis that " + guesses.get(1)+ "\n");
        concat += (" committed murder in the " + guesses.get(0) + " with the " + guesses.get(2) + "\n" + "\n");
        concat += ("- Player " + (1 + 1) + " refuted the hypothesis by showing the card \"" + "Card shown" + "\".\n"+"\n");
		assertEquals("Incorrect Notebook refute addition", my_notebook.show_notebook(), concat);
	}
}
