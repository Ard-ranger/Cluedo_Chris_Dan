package Tests;

import Cards.Card; 
import Cards.CardsFactory;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CardFactory_Tests {

	private CardsFactory my_cardfactory;
	private Card[] cards;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up cardfactory test class");
	}
	
	@Before
	public void setUp() {
		my_cardfactory = new CardsFactory();
	}
	
	@After
	public void tearDown() {
		my_cardfactory = null;
		cards = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down CardFactory Tests");
	}
	
	@Test
	public void test_cardsfactory_suspect_creation() {
		cards = my_cardfactory.getCards("SUSPECTS");
		
		assertEquals("Incorrect suspect card creation in cardfactory", cards[0].name(), "Professor Plum");
		//assertEquals("Incorrect suspect card creation in cardfactory", cards[5].name(), "Miss Scarlett");
	}
	
	@Test
	public void test_cardsfactory_room_creation() {
		cards = my_cardfactory.getCards("ROOM");
		
		assertEquals("Incorrect room card creation in cardfactory", cards[0].name(), "Conservatory");
		//assertEquals("Incorrect room card creation in cardfactory", cards[8].name(), "Billiard room");
	}
	
	@Test
	public void test_cardsfactory_weapon_creation() {
		cards = my_cardfactory.getCards("WEAPONS");
		
		assertEquals("Incorrect weapon card creation in cardfactory", "Candlestick", cards[0].name());
		//assertEquals("Incorrect weapon card creation in cardfactory", "Wrench", cards[5].name());
	} 
}
