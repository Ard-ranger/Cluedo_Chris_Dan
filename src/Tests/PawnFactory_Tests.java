package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Pawns.PawnFactory;

public class PawnFactory_Tests {
	
	private PawnFactory my_pawnfactory;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up PawnFactory test class");
	}
	
	@Before
	public void setUp() {
		my_pawnfactory = new PawnFactory();
	}
	
	@After
	public void tearDown() {
		my_pawnfactory = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down PawnFactory Tests");
	}
	
	@Test
	public void test_getting_correct_suspect_pawn_for_index() {
		assertEquals("Incorrect suspect pawn for index", my_pawnfactory.get_suspect_pawns(0).name(), "Professor Plum");
	}
	
	@Test
	public void test_getting_correct_weapon_pawn_for_index() {
		assertEquals("Incorrect weapon pawn for index", my_pawnfactory.get_weapon_pawns(0).name(), "Candlestick");
	}
}
