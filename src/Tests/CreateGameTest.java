package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Game.CreateGame;;

public class CreateGameTest {

	private CreateGame my_game;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up CreateGame Class");
	}
	
	@Before
	public void setUp() {
		my_game = new CreateGame(3);
	}
	
	@After
	public void tearDown() {
		my_game = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down CreateGame Tests");
	}
	
	@Test
	public void test_creategame_card_creation() {
		my_game.prepare_cards();
		assertEquals("Incorrect game cards creation", my_game.get_game_cards().size(), 21);
		assertEquals("Incorrect Envelope additions", my_game.get_envelope().number_of_cards(), 3);
		assertEquals("Incorrect PLayer card creation", my_game.get_player_cards().size(), 18);
	}
	
	@Test
	public void test_creategame_player_creation() {
		List<String> names = new ArrayList<String>();
		names.add("Chris");
		names.add("Dan");
		names.add("Phil");
		my_game.prepare_players(names);
		assertEquals("Incorrect player creation", my_game.get_players().get(0).get_name(), "Chris");
		assertEquals("Incorrect player creation", my_game.get_players().get(1).get_name(), "Dan");
		assertEquals("Incorrect player creation", my_game.get_players().get(2).get_name(), "Phil");
		assertEquals("Incorrect pawn assignment", my_game.get_players().get(0).player_pawn.name(), "Professor Plum");
	}
	
	@Test
	public void test_assigning_cards_to_players() {
		List<String> names = new ArrayList<String>();
		names.add("Chris");
		names.add("Dan");
		names.add("Phil");
		my_game.prepare_players(names);
		my_game.prepare_cards();
		
		my_game.assign_cards_to_players();
		assertEquals("Incorrect cards assigning", my_game.get_players().get(0).get_number_of_cards(), 6);
		assertEquals("Incorrect cards assigning", my_game.get_players().get(1).get_number_of_cards(), 6);
		assertEquals("Incorrect cards assigning", my_game.get_players().get(2).get_number_of_cards(), 6);
	}
	
	
}
