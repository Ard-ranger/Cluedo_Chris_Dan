package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Players.*;
import Pawns.*;
import Cards.*;


public class Player_Test {
	
	private Player player;
	private Card card;
	private Suspect_pawn pawn;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up player test class");
	}
	
	@Before
	public void setUp() {
		player = new Player(0, "Chris");
	}
	
	@After
	public void tearDown() {
		player = null;
		card = null;
		pawn = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Player Tests");
	}
	
	@Test
	public void test_assigning_card() {
		card = new Room_Card("Hall");
		player.assign_card(card);
		assertEquals("Card added to player incorrectly", player.get_number_of_cards(), 1);
		assertEquals("Card added to player incorrectly", player.get_cards(), card.name() + " " + card.type() + "\n");
	}
	
	@Test
	public void test_player_has_card() {
		card = new Room_Card("Hall");
		player.assign_card(card);
		assertEquals("Cannot see player has a card", player.have_card("Hall"), true);
	}
	
	@Test
	public void test_player_assigned_pawn() {
		assertEquals("Incorrect Pawn assignment", player.player_pawn.name(), "Professor Plum");
	}
	
	@Test
	public void test_player_notebook_additions() {
		player.add_to_notebook("Hall");
		assertEquals("Incorrect notebook addition from player", player.get_notebook(), "Hall" + "\n");
	}
	
	@Test
	public void test_player_is_playing() {
		player.player_lost();
		assertEquals("Player should not be playing", player.is_playing(), false);
	}
}
