package Tests;

import Cards.Weapon_Card;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Weapon_Card_Tests {
	
private Weapon_Card my_weapon_card;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Weapon_Card test class");
	}
	
	@Before
	public void setUp() {
		my_weapon_card = new Weapon_Card("Knife");
	}
	
	@After
	public void tearDown() {
		my_weapon_card = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Weapon_Card Tests");
	}

	@Test
	public void test_weapon_card_name() {
		assertEquals("Incorrect name assignment to weapon card", my_weapon_card.name(), "Knife");
	}
	
	@Test
	public void test_weapon_card_type() {
		assertEquals("Incorrect type assignment to weapon card", my_weapon_card.type(), "Weapon");
	}

}
