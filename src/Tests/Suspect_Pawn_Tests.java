package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Pawns.Suspect_pawn;

public class Suspect_Pawn_Tests {

	private Suspect_pawn my_suspect_pawn;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Suspect_Pawn test class");
	}
	
	@Before
	public void setUp() {
		my_suspect_pawn = new Suspect_pawn("Professor Plum", 1 , 2, 0);
	}
	
	@After
	public void tearDown() {
		my_suspect_pawn = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Suspect_Pawn Tests");
	}
	
	@Test
	public void test_suspect_pawn_type() {
		assertEquals("Incorrect type assignment to suspect pawn", my_suspect_pawn.type(), "Suspect");
	}
	
	@Test
	public void test_suspect_pawn_name() {
		assertEquals("Incorrect name assignment to suspect pawn", my_suspect_pawn.name(), "Professor Plum");
	}
	
	@Test
	public void test_suspect_pawn_correct_starting_position() {
		assertEquals("Incorrect x starting position for suspect pawn", my_suspect_pawn.getxPosition(), 1);
		assertEquals("Incorrect y starting position for suspect pawn", my_suspect_pawn.getyPosition(), 2);
	}
	
	@Test
	public void test_changing_xy_suspect_pawn_coordinates() {
		
		my_suspect_pawn.putxPosition(5);
		my_suspect_pawn.putyPosition(10);
		
		assertEquals("Incorrect x starting position for suspect pawn", my_suspect_pawn.getxPosition(), 5);
		assertEquals("Incorrect y starting position for suspect pawn", my_suspect_pawn.getyPosition(), 10);
	}
}
