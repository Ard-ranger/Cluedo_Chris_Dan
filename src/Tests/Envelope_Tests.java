package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Cards.*;
import Envelope.Envelope;

public class Envelope_Tests {

	private Room_Card my_room_card;
	private Weapon_Card my_weapon_card;
	private Suspect_Card my_suspect_card;
	private Envelope my_envelope;
	
	@BeforeClass
	public static void setupclass() {
		System.out.println("Setting up Envelope test class");
	}
	
	@Before
	public void setUp() {
		my_room_card = new Room_Card("Hall");
		my_weapon_card = new Weapon_Card("Knife");
		my_suspect_card = new Suspect_Card("Professor Plum");
		my_envelope = new Envelope();
	}
	
	@After
	public void tearDown() {
		my_room_card = null;
		my_weapon_card = null;
		my_suspect_card = null;
		my_envelope = null;
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Tearing down Envelope Tests");
	}
	
	@Test
	public void test_card_additions_are_added_to_envelope() {
		my_envelope.add_card(my_room_card);
		my_envelope.add_card(my_weapon_card);
		my_envelope.add_card(my_suspect_card);
		assertEquals("Card not correctly added to envelope", my_envelope.check_envelope(), "HallKnifeProfessor Plum");
	}

}
