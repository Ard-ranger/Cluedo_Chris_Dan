package Display;


import Slot.Slot;
import static java.lang.Thread.sleep;

public class Display {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String m = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";




    public static void print_unicode(Slot map[][]) {

        //Generating test board.

        System.out.println("\nEach room slot is represented by the the first two letters of its name, e.g. Ballroom is \"BA\".");
        System.out.println("Doors are represented by a green \"\uD83D\uDEAA\uD83D\uDEAA\" you must stand in the doorway of the room to make a guess in that room.\n");

        //Check every slot.
        int x = 0;
        int y = 22;
        String display_str = "";
        while (y > -1) {
            display_str = "";
            while (x < 23) {

                if ((map[x][y].Get_player_container()) != null) {
                    System.out.print(ANSI_RED + "" + map[x][y].get_index() + "" + map[x][y].get_index() + "" + ANSI_RESET);
                } else if (map[x][y].get_weapon_container() != null) {
                    System.out.print(ANSI_RED + map[x][y].get_weapon_icon() + ANSI_RESET);
                } else if (!map[x][y].getName().equals("Corridor") && !map[x][y].isIs_Wall()) {
                    System.out.print(ANSI_GREEN + "\uD83D\uDEAA\uD83D\uDEAA" + ANSI_RESET);
                } else if (map[x][y].getName().equals("Corridor")) {
                    System.out.print("--");
                } else if (map[x][y].getName().equals("Library")) {
                    System.out.print("LI");
                } else if (map[x][y].getName().equals("Billiard room")) {
                    System.out.print("BI");
                } else if (map[x][y].getName().equals("Conservatory")) {
                    System.out.print("CO");
                } else if (map[x][y].getName().equals("Ballroom")) {
                    System.out.print("BA");
                } else if (map[x][y].getName().equals("Kitchen")) {
                    System.out.print("KI");
                } else if (map[x][y].getName().equals("Dining room")) {
                    System.out.print("DI");
                } else if (map[x][y].getName().equals("Lounge")) {
                    System.out.print("LO");
                } else if (map[x][y].getName().equals("Hall")) {
                    System.out.print("HA");
                } else if (map[x][y].getName().equals("Study")) {
                    System.out.print("ST");
                }
                x++;
            }
            y--;
            System.out.print("\n");
            x = 0;
        }

    }









    public static void print_board(Slot map[][]) {
        //Generating test board.


        System.out.println("\nEach room slot is represented by the the first two letters of its name, e.g. Ballroom is \"BA\".");
        System.out.println("Doors are represented by \"DO\" you must stand in the doorway of the room to make a guess in that room.\n");


        int x = 22;
        int y = 22;
        String display_str = "";
        while (y > -1) {
            display_str = "";
            while (x > -1) {

                if ((map[x][y].Get_player_container()) != null) {
                    display_str = map[x][y].get_index() + "" + map[x][y].get_index() + display_str;
                } else if (!map[x][y].getName().equals("Corridor") && !map[x][y].isIs_Wall()) {
                    display_str = "DO" + display_str;
                } else if (map[x][y].getName().equals("Corridor")) {
                    display_str = "--" + display_str;
                } else if (map[x][y].getName().equals("Library")) {
                    display_str = "LL" + display_str;
                } else if (map[x][y].getName().equals("Billiard room")) {
                    display_str = "BI" + display_str;
                } else if (map[x][y].getName().equals("Conservatory")) {
                    display_str = "CO" + display_str;
                } else if (map[x][y].getName().equals("Ballroom")) {
                    display_str = "BA" + display_str;
                } else if (map[x][y].getName().equals("Kitchen")) {
                    display_str = "KK" + display_str;
                } else if (map[x][y].getName().equals("Dining room")) {
                    display_str = "DI" + display_str;
                } else if (map[x][y].getName().equals("Lounge")) {
                    display_str = display_str + "LO";
                } else if (map[x][y].getName().equals("Hall")) {
                    display_str = "HA" + display_str;
                } else if (map[x][y].getName().equals("Study")) {
                    display_str = "ST" + display_str;
                }
                x--;
            }
            System.out.println(display_str);
            y--;
            x = 22;
        }

    }


    public static void print_colour_board(Slot map[][]) {

        //Generating test board.

        System.out.println("\nEach room slot is represented by the the first two letters of its name, e.g. Ballroom is \"BA\".");
        System.out.println("Doors are represented by a green \"DD\" you must stand in the doorway of the room to make a guess in that room.\n");


        int x = 0;
        int y = 22;
        String display_str = "";
        while (y > -1) {
            display_str = "";
            while (x < 23) {

                if ((map[x][y].Get_player_container()) != null) {
                    System.out.print(ANSI_RED + "" + map[x][y].get_index() + "" + map[x][y].get_index() + "" + ANSI_RESET);
                } else if (!map[x][y].getName().equals("Corridor") && !map[x][y].isIs_Wall()) {
                    System.out.print(ANSI_GREEN + "DO" + ANSI_RESET);
                } else if (map[x][y].getName().equals("Corridor")) {
                    System.out.print("--");
                } else if (map[x][y].getName().equals("Library")) {
                    System.out.print("LI");
                } else if (map[x][y].getName().equals("Billiard room")) {
                    System.out.print("BI");
                } else if (map[x][y].getName().equals("Conservatory")) {
                    System.out.print("CO");
                } else if (map[x][y].getName().equals("Ballroom")) {
                    System.out.print("BA");
                } else if (map[x][y].getName().equals("Kitchen")) {
                    System.out.print("KI");
                } else if (map[x][y].getName().equals("Dining room")) {
                    System.out.print("DI");
                } else if (map[x][y].getName().equals("Lounge")) {
                    System.out.print("LO");
                } else if (map[x][y].getName().equals("Hall")) {
                    System.out.print("HA");
                } else if (map[x][y].getName().equals("Study")) {
                    System.out.print("ST");
                }
                x++;
            }
            y--;
            System.out.print("\n");
            x = 0;
        }

    }




}
