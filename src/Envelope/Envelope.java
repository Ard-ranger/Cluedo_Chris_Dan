package Envelope;

import Cards.Card;

// Envelope class stores one card of each type, these are the guilty murder suspect, murder weapon,
// and the murder room
public class Envelope {

    private Card[] envelope;
    private int i;        //Iterator

    // Initialise envelope to hold three cards
    public Envelope() {
        i = 0;
        envelope = new Card[3];
    }

    // Add a card to the envelope
    public void add_card(Card my_new_card) {
        envelope[i] = my_new_card;
        i++;
    }

    // Check whats in the envelope, returns a string
    public String check_envelope() {
        String str = envelope[0].name();
        str += envelope[1].name();
        str += envelope[2].name();
        return str;
    }

    public void print_envelope() {
        String str = "Correct hypothesis:\n Room: " + envelope[0].name() + " Character: " + envelope[1].name() + " Weapon: " + envelope[2].name() + "\n";
        System.out.println(str);
    }
    
    public int number_of_cards() {
    	return envelope.length;
    }
}